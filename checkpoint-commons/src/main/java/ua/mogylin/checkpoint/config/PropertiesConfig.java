package ua.mogylin.checkpoint.config;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

/**
 * Configuration of: <br/>
 * - properties<br/>
 * - property placeholder<br/>
 * - messageSource<br/>
 * 
 * @author oleksii.mogylin
 * 
 */
@Configuration
public class PropertiesConfig {

    private static final Logger LOG = Logger.getLogger(PropertiesConfig.class);

    public static final String MESSAGE_SOURCE = "classpath:global-messages";
    public static final String MESSAGE_ENCODING = "ISO-8859-1";
    public static final int MESSAGES_CACHE_PERIOD = 10;

    @Bean
    public Properties environmentProperties() {
        Properties props = new Properties();
        URL classPathEnvProperties = getClass().getResource("/app.properties");
        InputStream input = null;
        if (classPathEnvProperties != null) {
            LOG.info("Loading properties from classpath " + classPathEnvProperties.toString());
            try {
                input = getClass().getResourceAsStream("/app.properties");
                props.load(input);
                LOG.info("configured environmentProperties...");
            } catch (IOException e) {
                LOG.error("Error loading environment.properties from classpath ", e);
            } finally {
                IOUtils.closeQuietly(input);
            }
        } else {
            LOG.warn("Environment.properties not found on file or classpath");
        }
        return props;
    }

    @Bean
    public PropertyPlaceholderConfigurer propertyConfigurer() {
        PropertyPlaceholderConfigurer propertyPlaceholderConfigurer = new PropertyPlaceholderConfigurer();
        Properties properties = null;
        try {
            properties = environmentProperties();
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
        propertyPlaceholderConfigurer.setPropertiesArray(new Properties[] { properties });
        propertyPlaceholderConfigurer.setLocalOverride(true);
        propertyPlaceholderConfigurer.setIgnoreResourceNotFound(true);
        propertyPlaceholderConfigurer.setSystemPropertiesModeName("SYSTEM_PROPERTIES_MODE_OVERRIDE");
        LOG.info("configured propertyConfigurer...");
        return propertyPlaceholderConfigurer;
    }

    @Bean
    public MessageSource messageSource() {
        ReloadableResourceBundleMessageSource reloadableResourceBundleMessageSource = new ReloadableResourceBundleMessageSource();
        reloadableResourceBundleMessageSource.setBasename(MESSAGE_SOURCE);
        reloadableResourceBundleMessageSource.setDefaultEncoding(MESSAGE_ENCODING);
        reloadableResourceBundleMessageSource.setCacheSeconds(MESSAGES_CACHE_PERIOD);
        LOG.info("configured messageSource...");
        return reloadableResourceBundleMessageSource;
    }
}
