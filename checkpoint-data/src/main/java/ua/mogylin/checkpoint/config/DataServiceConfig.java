package ua.mogylin.checkpoint.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = { "ua.mogylin.checkpoint.service" })
public class DataServiceConfig {

}
