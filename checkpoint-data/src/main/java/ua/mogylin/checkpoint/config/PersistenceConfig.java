package ua.mogylin.checkpoint.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.sqlite.JDBC;

import ua.mogylin.checkpoint.db.hibernate.dialect.SQLiteDialect;

/**
 * Configuration of: <br/>
 * - dataSource<br/>
 * - JPA repositories<br/>
 * - entity and transaction managers<br/>
 * - etc...<br/>
 * 
 * @author oleksii.mogylin
 * 
 */
@Configuration
@EnableJpaRepositories(basePackages = { "ua.mogylin.checkpoint.repository" }, entityManagerFactoryRef = "entityManagerFactory", transactionManagerRef = "transactionManager")
@EnableTransactionManagement
public class PersistenceConfig {

    private static final Logger LOG = Logger.getLogger(PersistenceConfig.class);

    @Value("${db.url:jdbc:sqlite:D:\\MSSQL\\struts2_checkpoint.sqlite}")
    private String dbUrl;

    private Properties additionalProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.dialect", SQLiteDialect.class.getName());
        properties.put("hibernate.hbm2ddl.auto", "update");
        properties.put("hibernate.show_sql", "true");
        return properties;
    }

    @Bean
    public EntityManagerFactory entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean emFactoryBean = new LocalContainerEntityManagerFactoryBean();

        HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setShowSql(true);

        emFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);
        emFactoryBean.setDataSource(dataSource());
        emFactoryBean.setPackagesToScan("ua.mogylin.checkpoint.model");
        emFactoryBean.setJpaProperties(additionalProperties());
        emFactoryBean.afterPropertiesSet();
        EntityManagerFactory entityManagerFactory = emFactoryBean.getObject();
        LOG.info("configured entityManagerFactory...");
        return entityManagerFactory;
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(JDBC.class.getName());
        dataSource.setUrl(dbUrl);
        dataSource.setUsername("");
        dataSource.setPassword("");
        LOG.info("configured dataSource (" + dbUrl + ")...");
        return dataSource;
    }

    @Bean
    public JpaTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory());
        LOG.info("configured transactionManager...");
        return transactionManager;
    }

}
