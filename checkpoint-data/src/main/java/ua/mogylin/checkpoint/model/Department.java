package ua.mogylin.checkpoint.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@Entity
@XmlRootElement
public class Department {

    public static final Department PSEUDO_ROOT_DEPARTMENT = new Department(0,
            "[root]", 0);

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    private String name;

    @NotNull
    private Integer parentDepartmentId;

    public static Department byId(final Integer id) {
        return new Department(id);
    }

    public static Department copy(final Department origin, final Department dest) {
        dest.id = origin.id;
        dest.name = origin.name;
        dest.parentDepartmentId = origin.parentDepartmentId;
        return dest;
    }

    public Department() {
    }

    protected Department(final Integer id) {
        this.id = id;
    }

    protected Department(final Integer id, final String name, final Integer parent) {
        this.id = id;
        this.name = name;
        this.parentDepartmentId = parent;
    }

    protected Department(final Department other) {
        this.id = other.id;
        this.name = other.name;
        this.parentDepartmentId = other.parentDepartmentId;
    }

    public Integer getId() {
        return id;
    }
    public void setId(final Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    @XmlElement(name = "parentId")
    public Integer getParentDepartmentId() {
        return parentDepartmentId;
    }

    public void setParentDepartmentId(final Integer parentDepartmentId) {
        this.parentDepartmentId = parentDepartmentId;
    }

    @Override
    public String toString() {
        return "Department[id=" + id + ",name=" + name + ",parent="
                + this.parentDepartmentId + "]";
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(final Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }
}
