package ua.mogylin.checkpoint.model;

public enum IdType {
    PASSPORT, FOREIGN_PASSPORT, DRIVERS_LICENSE;
}
