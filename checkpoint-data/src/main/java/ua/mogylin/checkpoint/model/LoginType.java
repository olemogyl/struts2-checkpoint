package ua.mogylin.checkpoint.model;

public enum LoginType {
    USER, ADMIN;
}
