package ua.mogylin.checkpoint.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;

    @NotNull
    @OneToOne
    @JoinColumn(name = "departmentId")
    private Department department;

    @NotNull
    private Integer yearBorn;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "person", cascade = { CascadeType.REMOVE })
    private User login;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return this.lastName + ", " + this.firstName;
    }

    public Integer getYearBorn() {
        return yearBorn;
    }

    public void setYearBorn(final Integer yearBorn) {
        this.yearBorn = yearBorn;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(final Department department) {
        this.department = department;
    }

    public User getLogin() {
        return login;
    }
}
