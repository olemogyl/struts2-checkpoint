package ua.mogylin.checkpoint.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

@Entity
public class User extends Login {

    @NotNull
    @OneToOne
    @JoinColumn(name = "personId", unique = true)
    private Person person;

    @NotNull
    private LoginType role;

    public LoginType getRole() {
        return role;
    }

    public void setRole(final LoginType role) {
        this.role = role;
    }
    public Person getPerson() {
        return person;
    }
    public void setPerson(final Person person) {
        this.person = person;
    }


}
