package ua.mogylin.checkpoint.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ua.mogylin.checkpoint.model.Department;

public interface DepartmentRepository extends
 JpaRepository<Department, Integer> {

    @Query("SELECT dept FROM Department dept WHERE dept.name = :name")
    Department findByName(@Param("name") final String name);
}
