package ua.mogylin.checkpoint.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ua.mogylin.checkpoint.model.Guest;

public interface GuestRepository extends JpaRepository<Guest, Integer> {

}
