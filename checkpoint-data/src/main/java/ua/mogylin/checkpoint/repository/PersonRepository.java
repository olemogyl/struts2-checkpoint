package ua.mogylin.checkpoint.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ua.mogylin.checkpoint.model.Person;

public interface PersonRepository extends JpaRepository<Person, Integer> {

    @Query("SELECT persona FROM Person persona WHERE persona.firstName = :firstname and persona.lastName = :lastname")
    Person findByName(@Param("firstname") final String firstName, @Param("lastname") final String lastName);

    @Query("SELECT persona FROM Person persona WHERE persona.lastName = :lastname")
    Person findByLastName(@Param("lastname") final String lastName);

}
