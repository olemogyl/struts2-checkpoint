package ua.mogylin.checkpoint.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import ua.mogylin.checkpoint.model.User;

public interface UserRepository extends JpaRepository<User, String> {

    @Query("SELECT user FROM User user WHERE user.person.id = :personId")
    User getByPersonId(@Param("personId") final Integer personId);

    @Query("SELECT user FROM User user WHERE user.login = :login")
    User getByLogin(@Param("login") final String login);
}
