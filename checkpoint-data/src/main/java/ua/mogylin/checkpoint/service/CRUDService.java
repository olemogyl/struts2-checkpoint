package ua.mogylin.checkpoint.service;

import java.util.List;
import java.util.Map;

public interface CRUDService<T, V> {
    T add(T obj);

    void save(T obj);

    void remove(T obj);

    T get(V key);

    List<T> getAll();

    Map<V, T> getAllToMap();
}
