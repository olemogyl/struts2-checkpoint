package ua.mogylin.checkpoint.service;

import ua.mogylin.checkpoint.model.Department;

public interface DepartmentService extends CRUDService<Department, Integer> {

    Department getByName(final String name);

}
