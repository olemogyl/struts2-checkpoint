package ua.mogylin.checkpoint.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.mogylin.checkpoint.model.Department;
import ua.mogylin.checkpoint.repository.DepartmentRepository;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    private static final Logger LOG = Logger
            .getLogger(DepartmentServiceImpl.class
                    .getName());

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public Department add(final Department department) {
        Department savedDepartment = departmentRepository.save(department);
        LOG.info("Added department: " + savedDepartment);
        return savedDepartment;
    }

    @Override
    public void save(final Department department) {
        LOG.info("Updated department: " + department);
        departmentRepository.save(department);
    }

    @Override
    public List<Department> getAll() {
        ArrayList<Department> result = new ArrayList<Department>();
        result.addAll(departmentRepository.findAll());
        LOG.info("Departments found: " + result.size());
        return result;
    }

    @Override
    public void remove(final Department department) {
        Integer departmentId = department.getId();
        LOG.info("Removed department: " + departmentId);
        departmentRepository.delete(departmentId);
    }

    @Override
    public Department getByName(final String name) {
        return departmentRepository.findByName(name);
    }

    @Override
    public Department get(final Integer departmentId) {
        Department department = departmentRepository.findOne(departmentId);
        LOG.info("Found department: " + department);
        return department;
    }

    @Override
    public Map<Integer, Department> getAllToMap() {
        Map<Integer, Department> result = new HashMap<Integer, Department>();
        for (Department dept : this.getAll()) {
            result.put(dept.getId(), dept);
        }
        return result;
    }

}
