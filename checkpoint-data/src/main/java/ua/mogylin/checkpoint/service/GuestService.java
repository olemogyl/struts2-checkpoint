package ua.mogylin.checkpoint.service;

import ua.mogylin.checkpoint.model.Guest;

public interface GuestService extends CRUDService<Guest, Integer> {

}
