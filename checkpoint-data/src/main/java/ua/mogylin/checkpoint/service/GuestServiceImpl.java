package ua.mogylin.checkpoint.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.mogylin.checkpoint.model.Guest;
import ua.mogylin.checkpoint.repository.GuestRepository;

@Service
public class GuestServiceImpl implements GuestService {

    @Autowired
    private GuestRepository guestRepository;

    @Override
    public Guest add(final Guest obj) {
        return guestRepository.save(obj);
    }

    @Override
    public void save(final Guest obj) {
        guestRepository.save(obj);
    }

    @Override
    public void remove(final Guest obj) {
        guestRepository.delete(obj);
    }

    @Override
    public Guest get(final Integer key) {
        return guestRepository.findOne(key);
    }

    @Override
    public List<Guest> getAll() {
        return guestRepository.findAll();
    }

    @Override
    public Map<Integer, Guest> getAllToMap() {
        Map<Integer, Guest> result = new HashMap<Integer, Guest>();
        for (Guest guest : this.getAll()) {
            result.put(guest.getId(), guest);
        }
        return result;
    }

}
