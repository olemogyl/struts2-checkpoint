package ua.mogylin.checkpoint.service;

import ua.mogylin.checkpoint.model.Person;

public interface PersonService extends CRUDService<Person, Integer> {

    Person getByName(final String firstName, final String lastName);

    Person getByLastName(final String lastName);
}
