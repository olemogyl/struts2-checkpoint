package ua.mogylin.checkpoint.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.mogylin.checkpoint.model.Person;
import ua.mogylin.checkpoint.repository.PersonRepository;

@Service
public class PersonServiceImpl implements PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Override
    public Person add(final Person obj) {
        return personRepository.save(obj);
    }

    @Override
    public void save(final Person obj) {
        personRepository.save(obj);
    }

    @Override
    public void remove(final Person obj) {
        personRepository.delete(obj.getId());
    }

    @Override
    public Person get(final Integer key) {
        return personRepository.findOne(key);
    }

    @Override
    public List<Person> getAll() {
        return personRepository.findAll();
    }

    @Override
    public Person getByName(final String firstName, final String lastName) {
        return personRepository.findByName(firstName, lastName);
    }

    @Override
    public Person getByLastName(final String lastName) {
        return personRepository.findByLastName(lastName);
    }

    @Override
    public Map<Integer, Person> getAllToMap() {
        Map<Integer, Person> result = new HashMap<Integer, Person>();
        for (Person person : this.getAll()) {
            result.put(person.getId(), person);
        }
        return result;
    }
}
