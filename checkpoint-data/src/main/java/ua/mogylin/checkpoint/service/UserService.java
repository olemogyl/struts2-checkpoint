package ua.mogylin.checkpoint.service;

import ua.mogylin.checkpoint.model.Login;
import ua.mogylin.checkpoint.model.User;

public interface UserService extends CRUDService<User, String> {

    User getByPersonId(final Integer personId);

    boolean checkLoginValid(final Login login);

    String getUserRole(final Login login);
}
