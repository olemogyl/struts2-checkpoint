package ua.mogylin.checkpoint.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ua.mogylin.checkpoint.model.Login;
import ua.mogylin.checkpoint.model.User;
import ua.mogylin.checkpoint.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public boolean checkLoginValid(final Login login) {
        if (login == null) {
            return false;
        }
        User user = userRepository.getByLogin(login.getLogin());
        return user.getPassword().equals(login.getPassword());
    }

    @Override
    public String getUserRole(final Login login) {
        // TODO Auto-generated method stub
        return null;

    }

    @Override
    public User add(final User obj) {
        return userRepository.save(obj);
    }

    @Override
    public void save(final User obj) {
        userRepository.save(obj);
    }

    @Override
    public void remove(final User obj) {
        userRepository.delete(obj.getLogin());
    }

    @Override
    public User get(final String key) {
        return userRepository.getOne(key);
    }

    @Override
    public User getByPersonId(final Integer personId) {
        return userRepository.getByPersonId(personId);
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public Map<String, User> getAllToMap() {
        Map<String, User> result = new HashMap<String, User>();
        for (User person : this.getAll()) {
            result.put(person.getLogin(), person);
        }
        return result;
    }

}
