package ua.mogylin.checkpoint.rest.client;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;

import ua.mogylin.checkpoint.model.Department;

public class DepartmentClient {

    private Client client;

    public DepartmentClient() {
        client = ClientBuilder.newClient();
    }

    public Department get(final Integer id) {
        WebTarget target = client.target("http://localhost:8080/checkpoint/webapi/");

        Department result = target.path("departments/" + id).request().get(Department.class);

        return result;
    }

}
