package ua.mogylin.checkpoint.rest.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import ua.mogylin.checkpoint.config.DataServiceConfig;
import ua.mogylin.checkpoint.config.PersistenceConfig;
import ua.mogylin.checkpoint.config.PropertiesConfig;

@Configuration
@Import({ PropertiesConfig.class, PersistenceConfig.class, DataServiceConfig.class })
public class ApplicationConfig {


}
