package ua.mogylin.checkpoint.rest.jersey;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ua.mogylin.checkpoint.model.Department;
import ua.mogylin.checkpoint.service.DepartmentService;

@Component
@Scope("prototype")
@Path("departments")
public class DepartmentResource {
    @Inject
    private DepartmentService departmentService;

    @GET
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public List<Department> getAll() {
        return departmentService.getAll();
    }

    @GET
    @Path("{id}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Department get(@PathParam("id") final Integer id) {
        return departmentService.get(id);
    }

    @POST
    @Path("department")
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Department addDepartment(final Department department) {
        return departmentService.add(department);
    }
}
