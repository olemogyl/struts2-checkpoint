package ua.mogylin.checkpoint.rest.jersey;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ua.mogylin.checkpoint.model.Guest;
import ua.mogylin.checkpoint.model.Person;
import ua.mogylin.checkpoint.service.GuestService;

@Component
@Scope("prototype")
@Path("guests")
public class GuestResource {

    @Inject
    private GuestService guestService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Guest> show() {
        return guestService.getAll();
    }

    @GET
    @Path("{id}/host")
    @Produces(MediaType.APPLICATION_JSON)
    public Person getHost(@PathParam("id") final Integer id) {
        return guestService.get(id).getHost();
    }

}
