/**
 * This class has been generated by Fast Code Eclipse Plugin
 * For more information please go to http://fast-code.sourceforge.net/
 * @author : oleksii.mogylin
 * Created : 09/04/2014 03:25:32
 */

package ua.mogylin.checkpoint.rest.client;

import static org.junit.Assert.assertNotNull;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ua.mogylin.checkpoint.model.Department;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContextBootstrap.xml" })
@Ignore
public class DepartmentClientTest {

    private DepartmentClient departmentClient = new DepartmentClient();

    /**
     *
     * @see ua.mogylin.checkpoint.rest.client.DepartmentClient#get(Integer)
     */
    @Test
    public void get() {
        Department t = departmentClient.get(1);
        assertNotNull(" cannot be null", t);
    }

}
