package ua.mogylin.checkpoint.rest.dw;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import javax.validation.Validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ConfigurableWebApplicationContext;

import ua.mogylin.checkpoint.rest.dw.config.ApplicationConfig;
import ua.mogylin.checkpoint.rest.dw.spring.SpringContextBootstrapper;

import com.sun.jersey.api.core.ResourceConfig;

public class App extends Application<AppConfigurationProperties> {

    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    @Override
    public void initialize(final Bootstrap<AppConfigurationProperties> b) {
        LOGGER.error("Method App#initialize() called");
    }

    @Override
    public void run(final AppConfigurationProperties c, final Environment e) throws Exception {
        LOGGER.error("Method App#run() called");

        e.jersey().getResourceConfig().getFeatures().put(ResourceConfig.FEATURE_DISABLE_WADL, Boolean.FALSE);

        // Load spring context
        ConfigurableWebApplicationContext context = new SpringContextBootstrapper(c, e).bootstrap(ApplicationConfig.class);

        Validator validator = e.getValidator();
        context.getBeanFactory().registerSingleton("validator", validator);
    }



    public static void main(final String[] args) throws Exception {
        new App().run(args);
    }
}
