package ua.mogylin.checkpoint.rest.dw;

import io.dropwizard.Configuration;
import io.dropwizard.db.DataSourceFactory;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AppConfigurationProperties extends Configuration {

    @JsonProperty
    private String message;

    @JsonProperty
    private int messageRepetitions;

    @JsonProperty
    private DataSourceFactory database = new DataSourceFactory();

    public String getMessage() {
        return message;
    }

    public int getMessageRepetitions() {
        return messageRepetitions;
    }

    public DataSourceFactory getDataSourceFactory() {
        return database;
    }

}
