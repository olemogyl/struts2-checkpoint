package ua.mogylin.checkpoint.rest.dw.config.jdbi;

import org.skife.jdbi.v2.DBI;
import org.springframework.web.context.ConfigurableWebApplicationContext;

import ua.mogylin.checkpoint.rest.dw.dao.DepartmentDao;

public class DaoSpringConfig {

    /**
     * Registers all JDBI daos as Spring beans.<br/>
     * Must use @Lazy on all placed where Dao instances are going to be
     * autowired
     * 
     * @param jdbi
     * @param springContext
     */
    public static void registerDaoInSpring(final DBI jdbi, final ConfigurableWebApplicationContext springContext) {
        // here we can read all DAO classes with reflection
        DepartmentDao dao = jdbi.onDemand(DepartmentDao.class);
        springContext.getBeanFactory().registerSingleton("departmentDao", dao);
    }

}
