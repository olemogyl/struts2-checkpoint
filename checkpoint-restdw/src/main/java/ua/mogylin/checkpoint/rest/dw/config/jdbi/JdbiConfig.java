package ua.mogylin.checkpoint.rest.dw.config.jdbi;

import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Environment;

import org.skife.jdbi.v2.DBI;

/**
 * Configuration for Dropwizard JDBI
 * 
 * @author oleksii.mogylin
 * 
 */
public class JdbiConfig {

    public static DBI buildJdbi(final Environment environment, final DataSourceFactory dataSourceFactory) throws ClassNotFoundException {
        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, dataSourceFactory, "sqlite");
        return jdbi;
    }

}
