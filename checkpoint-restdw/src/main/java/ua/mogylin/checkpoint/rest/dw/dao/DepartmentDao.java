package ua.mogylin.checkpoint.rest.dw.dao;

import java.util.List;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.customizers.Mapper;

import ua.mogylin.checkpoint.model.Department;
import ua.mogylin.checkpoint.rest.dw.dao.mappers.DepartmentMapper;

public interface DepartmentDao {

    @Mapper(DepartmentMapper.class)
    @SqlQuery("select * from Department where id = :id")
    Department get(@Bind("id") final Integer id);

    @Mapper(DepartmentMapper.class)
    @SqlQuery("select * from Department")
    List<Department> getAll();

}
