package ua.mogylin.checkpoint.rest.dw.dao.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import ua.mogylin.checkpoint.model.Department;

public class DepartmentMapper implements ResultSetMapper<Department> {

    @Override
    public Department map(final int index, final ResultSet resultSet, final StatementContext stmtCtx) throws SQLException {
        Department result = new Department();
        result.setId(resultSet.getInt("id"));
        result.setName(resultSet.getString("name"));
        result.setParentDepartmentId(resultSet.getInt("parentDepartmentId"));
        return result;
    }

}
