package ua.mogylin.checkpoint.rest.dw.representation;

import io.dropwizard.validation.ValidationMethod;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import ua.mogylin.checkpoint.model.Department;
import ua.mogylin.checkpoint.rest.dw.validation.constraint.ValidDepartmentName;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "id", "name" })
@JsonAutoDetect(isGetterVisibility = Visibility.PUBLIC_ONLY)
public class DepartmentRest implements RestRepresentation<Department> {

    public static DepartmentRest wrap(final Department department) {
        return new DepartmentRest(department);
    }

    public static List<DepartmentRest> wrap(final List<Department> departments) {
        if (departments == null) {
            return null;
        }
        List<DepartmentRest> list = new ArrayList<DepartmentRest>(departments.size());
        for (Department department : departments) {
            list.add(wrap(department));
        }
        return list;
    }

    @Valid
    @JsonIgnore
    private Department delegate = new Department();

    public DepartmentRest() {

    }

    private DepartmentRest(final Department department) {
        this.delegate = department;
    }

    public Integer getId() {
        return delegate.getId();
    }

    public void setId(final Integer id) {
        delegate.setId(id);
    }

    @ValidDepartmentName
    public String getName() {
        return delegate.getName();
    }

    public void setName(final String name) {
        delegate.setName(name);
    }

    public Integer getParentDepartmentId() {
        return delegate.getParentDepartmentId();
    }

    @JsonProperty("parentId")
    public void setParentDepartmentId(final Integer parentDepartmentId) {
        delegate.setParentDepartmentId(parentDepartmentId);
    }

    @Override
    public Department entity() {
        return delegate;
    }

    @Override
    public void setEntity(final Department obj) {
        this.delegate = obj;
    }

    @ValidationMethod
    protected boolean isNotParentForItself() {
        Integer parentDepartmentId = delegate.getParentDepartmentId();
        return parentDepartmentId != null && !parentDepartmentId.equals(delegate.getId());
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(final Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

}
