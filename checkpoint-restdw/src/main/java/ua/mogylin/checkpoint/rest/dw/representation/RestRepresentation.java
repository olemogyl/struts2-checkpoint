package ua.mogylin.checkpoint.rest.dw.representation;

/**
 * 
 * @author oleksii.mogylin
 * 
 * @param <T>
 *            is an entity object
 */
public interface RestRepresentation<T> {

    /**
     * Constructs entity object in order to persist it.
     * 
     * @return
     */
    T entity();

    void setEntity(final T obj);
}
