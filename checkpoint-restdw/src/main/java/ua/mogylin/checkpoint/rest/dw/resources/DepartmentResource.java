package ua.mogylin.checkpoint.rest.dw.resources;

import java.util.List;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import ua.mogylin.checkpoint.model.Department;
import ua.mogylin.checkpoint.rest.dw.representation.DepartmentRest;
import ua.mogylin.checkpoint.service.DepartmentService;

@Component
@Scope("prototype")
@Path("/departments")
@Produces(MediaType.APPLICATION_JSON)
public class DepartmentResource implements RestResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentResource.class);

    @Autowired
    private DepartmentService departmentService;

    @GET
    public List<DepartmentRest> getAll() {
        LOGGER.info("DepartmentResource#getAll() called");
        return DepartmentRest.wrap(departmentService.getAll());
    }

    @GET
    @Path(ID_PATH)
    public Response get(final @PathParam(ID) Integer id) {
        LOGGER.info("DepartmentResource#get() called for " + id);
        Department department = departmentService.get(id);
        if (department == null) {
            return Response.status(Status.NOT_FOUND).build();
        }
        return Response.ok().entity(DepartmentRest.wrap(department)).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public DepartmentRest addDepartment(final @Valid DepartmentRest department) {
        LOGGER.info("DepartmentResource#addDepartment() called");
        return DepartmentRest.wrap(departmentService.add(department.entity()));
    }

    @PUT
    @Path(ID_PATH)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response changeDepartment(final @Valid DepartmentRest department, final @PathParam(ID) Integer id) {
        LOGGER.info("DepartmentResource#addDepartment() called");
        Department dept = departmentService.get(id);
        if (dept == null) {
            return Response.status(Status.NOT_FOUND).build();
        }
        Department toBeSaved = department.entity();
        toBeSaved.setId(id);
        departmentService.save(toBeSaved);
        return Response.ok().entity(DepartmentRest.wrap(departmentService.get(id))).build();
    }

    @DELETE
    @Path(ID_PATH)
    public Response delete(final @PathParam(ID) Integer id) {
        LOGGER.info("DepartmentResource#get() called");
        Department dept = departmentService.get(id);
        if (dept != null) {
            departmentService.remove(dept);
        }
        return Response.ok().build();
    }

    void setDepartmentService(final DepartmentService departmentService) {
        this.departmentService = departmentService;
    }
}
