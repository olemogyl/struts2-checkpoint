package ua.mogylin.checkpoint.rest.dw.resources;

public interface RestResource {

    public static final String ID_PATH = "/{id}";

    public static final String ID = "id";

}
