package ua.mogylin.checkpoint.rest.dw.spring;

import io.dropwizard.Configuration;
import io.dropwizard.servlets.tasks.Task;
import io.dropwizard.setup.Environment;

import java.util.Map;

import javax.ws.rs.Path;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ConfigurableWebApplicationContext;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

import com.codahale.metrics.health.HealthCheck;

public class SpringContextBootstrapper {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpringContextBootstrapper.class);

    private Environment environment;
    private Configuration configuration;

    public SpringContextBootstrapper(final Configuration configuration, final Environment environment) {
        this.configuration = configuration;
        this.environment = environment;
    }

    public ConfigurableWebApplicationContext bootstrap(final Class<?> contextConfigRoot) {

        // init Spring context
        // before we init the app context, we have to create a parent context
        // with all the config objects others rely on to get initialized
        AnnotationConfigWebApplicationContext parent = new AnnotationConfigWebApplicationContext();
        AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();
        parent.refresh();
        parent.getBeanFactory().registerSingleton("configuration", configuration);
        parent.registerShutdownHook();
        parent.start();
        // the real main app context has a link to the parent context
        ctx.setParent(parent);
        ctx.register(contextConfigRoot);
        ctx.refresh();
        ctx.registerShutdownHook();
        ctx.start();
        // now that Spring is started, let's get all the beans that matter into
        // DropWizard
        // health checks
        Map<String, HealthCheck> healthChecks = ctx.getBeansOfType(HealthCheck.class);
        for (Map.Entry<String, HealthCheck> entry : healthChecks.entrySet()) {
            LOGGER.info("found HEALTHCHECK: " + entry.getKey());
            environment.healthChecks().register(entry.getKey(), entry.getValue());
        }
        // resources
        Map<String, Object> resources = ctx.getBeansWithAnnotation(Path.class);
        for (Map.Entry<String, Object> entry : resources.entrySet()) {
            LOGGER.info("found JERSEY RESOURCE: " + entry.getKey());
            environment.jersey().register(entry.getValue());
        }
        // tasks
        Map<String, Task> tasks = ctx.getBeansOfType(Task.class);
        for (Map.Entry<String, Task> entry : tasks.entrySet()) {
            LOGGER.info("found TASK: " + entry.getKey());
            environment.admin().addTask(entry.getValue());
        }
        // JAX-RS providers
        Map<String, Object> providers = ctx.getBeansWithAnnotation(Provider.class);
        for (Map.Entry<String, Object> entry : providers.entrySet()) {
            LOGGER.info("found JERSEY PROVIDER: " + entry.getKey());
            environment.jersey().register(entry.getValue());
        }
        // last, but not least, let's link Spring to the embedded Jetty in
        // Dropwizard
        environment.servlets().addServletListeners(new SpringContextLoaderListener(ctx));
        // activate Spring Security filter
        // new EnumSet<DispatcherType>();
        // environment.servlets().addFilter("springSecurityFilterChain",
        // DelegatingFilterProxy.class)
        // .addMappingForUrlPatterns(DispatcherType.REQUEST, false, "/*");
        return ctx;
    }


}
