package ua.mogylin.checkpoint.rest.dw;

import static org.fest.assertions.api.Assertions.assertThat;
import io.dropwizard.testing.junit.DropwizardAppRule;

import java.io.IOException;
import java.io.StringWriter;

import javax.ws.rs.core.MediaType;

import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Lazy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import ua.mogylin.checkpoint.model.Department;
import ua.mogylin.checkpoint.rest.dw.App;
import ua.mogylin.checkpoint.rest.dw.AppConfigurationProperties;
import ua.mogylin.checkpoint.rest.dw.testconfig.DummyConfig;
import ua.mogylin.checkpoint.service.DepartmentService;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DummyConfig.class)
public class AppTest implements ApplicationContextAware {

    private static final String SERVER_URL = "http://localhost:8080/";
    private static final String RESOURCE_URL = SERVER_URL + "departments";

    private Client client;

    @Autowired
    @Lazy
    private DepartmentService departmentService;
    private ApplicationContext appContext;

    @ClassRule
    public static final DropwizardAppRule<AppConfigurationProperties> RULE = new DropwizardAppRule<AppConfigurationProperties>(App.class, "config.yaml");


    @Before
    public void setUp() {
        client = new Client();
        // Set the credentials to be used by the client
        // client.addFilter(new HTTPBasicAuthFilter("wsuser", "wsp1"));
    }

    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException {
        this.appContext = applicationContext;
    }

    @Test
    public void createAndRetrieveContact() throws IOException {

        assertThat(departmentService != null);
        // Create a new contact by performing the appropriate
        //http request (POST)
        ClientResponse response = addDepartment(new Department());
        // Check that the response has the appropriate
        //        response code (201)
        assertThat(response.getStatus()).isEqualTo(422);


        Department department = new Department();
        department.setName("testeste");
        department.setParentDepartmentId(0);
        response = addDepartment(department);
        // Check that the response has the appropriate
        // response code (201)
        assertNoValidation(response);
        assertThat(response.getStatus()).isEqualTo(200);

        // Retrieve the newly created contact
        String newContactURL =
                response.getHeaders().get("Location").get(0);
        WebResource newContactResource =
                client.resource(newContactURL);
        Department contact =
                newContactResource.get(Department.class);
        // Check that it has the same properties
        //        as the initial one
        //        assertThat(contact.getFirstName()).
        //        isEqualTo(contactForTest.getFirstName());
        //        assertThat(contact.getLastName()).isEqualTo
        //        (contactForTest.getLastName());
        //        assertThat(contact.getPhone()).isEqualTo
        //        (contactForTest.getPhone());
    }

    private ClientResponse addDepartment(final Department department) {
        ClientResponse response;
        WebResource contactResource = client.resource(RESOURCE_URL);
        return contactResource
                .type(MediaType.APPLICATION_JSON)
                .post(ClientResponse.class, department);
    }


    public static void assertNoValidation(final ClientResponse response) throws IOException {
        if (response.getStatus() == 422) {
            StringWriter writer = new StringWriter();
            IOUtils.copy(response.getEntityInputStream(), writer, "UTF-8");
            throw new org.junit.ComparisonFailure("Response has validation messages", "<no exceptions>", writer.toString());
        }
    }


}
