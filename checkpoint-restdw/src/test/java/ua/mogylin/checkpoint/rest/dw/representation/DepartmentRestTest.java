package ua.mogylin.checkpoint.rest.dw.representation;
import static io.dropwizard.testing.FixtureHelpers.fixture;
import static org.fest.assertions.api.Assertions.assertThat;
import io.dropwizard.jackson.Jackson;

import java.io.IOException;

import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import com.fasterxml.jackson.databind.ObjectMapper;

public class DepartmentRestTest {

    private static final String FIXTURES_FOLDER = "fixtures/departments/";

    private static String fixtureFromFile(final String fixtureFile) throws IOException {
        return fixture(FIXTURES_FOLDER + fixtureFile);
    }

    private static final ObjectMapper MAPPER = Jackson.newObjectMapper();

    @Test
    public void serializesToJSON() throws Exception {

        final DepartmentRest person = new DepartmentRest();
        String data = MAPPER.writeValueAsString(person);
        String expected = fixtureFromFile("empty.json");
        JSONAssert.assertEquals(expected, data, false);
    }

    @Test
    public void deserializesFromJSON() throws Exception {
        final DepartmentRest person = new DepartmentRest();
        assertThat(MAPPER.readValue(fixtureFromFile("empty.json"), DepartmentRest.class)).isEqualTo(person);
    }
}
