package ua.mogylin.checkpoint.rest.dw.resources;

import ua.mogylin.checkpoint.service.DepartmentService;

public class DepartmentResourceBuilder {

    private DepartmentResource resource = new DepartmentResource();

    DepartmentResourceBuilder setDepartmentService(final DepartmentService departmentService) {
        resource.setDepartmentService(departmentService);
        return this;
    }

    public DepartmentResource build() {
        return this.resource;
    }
}
