package ua.mogylin.checkpoint.rest.dw.resources;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import io.dropwizard.testing.junit.ResourceTestRule;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;

import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.Mockito;

import ua.mogylin.checkpoint.model.Department;
import ua.mogylin.checkpoint.rest.dw.representation.DepartmentRest;
import ua.mogylin.checkpoint.service.DepartmentService;

public class DepartmentResourceTest {

    private static final DepartmentService dao = mock(DepartmentService.class);

    private static final DepartmentResource resource = new DepartmentResourceBuilder().setDepartmentService(dao).build();

    @ClassRule
    public static final ResourceTestRule resources = ResourceTestRule.builder().addResource(resource)
    .build();

    @Before
    public void setup() {
        Mockito.reset(dao);
    }

    @Test
    public void testGetPerson() {
        Department department = new Department();
        when(dao.get(eq(0))).thenReturn(department);
        DepartmentRest departmentRest = DepartmentRest.wrap(department);
        assertThat(resources.client().resource("/departments/0").get(DepartmentRest.class)).isEqualTo(departmentRest);
        verify(dao).get(0);
    }

    @Test(expected = ConstraintViolationException.class)
    public void testSavePerson() {

        Department department = new Department();
        department.setId(0);
        department.setName("");
        department.setParentDepartmentId(1);
        DepartmentRest departmentRest = DepartmentRest.wrap(department);

        try {
            resources.client().resource("/departments").type(MediaType.APPLICATION_JSON).post(DepartmentRest.class, departmentRest);
        } catch (ConstraintViolationException ex) {
            assertViolationsNumber(ex.getConstraintViolations(), 1);
            assertNextViolation(ex.getConstraintViolations(), "name", "Invalid department name");
            verify(dao, times(0)).get(anyInt());
            throw ex;
        }
    }

    private static void assertNextViolation(final Set<ConstraintViolation<?>> constraintViolations, final String offendingPath, final String validationMessage) {
        ConstraintViolation<?> violation = constraintViolations.iterator().next();
        org.junit.Assert.assertThat(violation.getPropertyPath().toString(), equalTo(offendingPath));
        org.junit.Assert.assertThat(violation.getMessage(), equalTo(validationMessage));
    }

    private static void assertViolationsNumber(final Set<ConstraintViolation<?>> constraintViolations, final int expectedSize) {
        org.junit.Assert.assertThat("One violation expected", constraintViolations.size(), equalTo(expectedSize));
    }

}
