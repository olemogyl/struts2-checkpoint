package ua.mogylin.checkpoint.aspect;

import org.aspectj.lang.annotation.Pointcut;

public final class AnyServicePointcut {

    private AnyServicePointcut() {
    }

    @Pointcut("execution(* ua.mogylin..*Service.*(..))")
    public void match() {
    }
}
