package ua.mogylin.checkpoint.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class TracingAspect {

    Logger logger = Logger.getLogger(TracingAspect.class);

    @Before("AnyServicePointcut.match()")
    public void entering(final JoinPoint p) {
        logger.info("entering service method " + p.getStaticPart().getSignature().toString());
    }

    @AfterThrowing(pointcut = "execution(* getAll())", throwing = "ex")
    public void exception(final Exception ex) {
        logger.error("exception ", ex);
    }
}
