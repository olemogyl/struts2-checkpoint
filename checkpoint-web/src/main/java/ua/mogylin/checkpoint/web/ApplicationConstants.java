package ua.mogylin.checkpoint.web;

/**
 * Constants used across the application.
 * 
 * @author oleksii.mogylin
 * 
 */
public interface ApplicationConstants {
    final String DEFAULT_PACKAGE = "webAppDefaultPackage";
    final String DESK_PACKAGE = "webAppDeskPackage";
    final String APP_ADMIN_PACKAGE = "webAppAdminPackage";

    final String ROOT_NAMESPACE = "/";
    final String NS_ADMIN_ROOT = "/admin";
    final String NS_ADMIN_DEPARMENTS = "/admin/departments";

    final String DEFAULT_ROOT_VIEW = "index.jsp";
}
