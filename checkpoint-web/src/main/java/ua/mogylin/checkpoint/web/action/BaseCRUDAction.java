package ua.mogylin.checkpoint.web.action;

import org.apache.log4j.Logger;
import org.apache.struts2.interceptor.validation.SkipValidation;

import ua.mogylin.checkpoint.service.CRUDService;

import com.opensymphony.xwork2.ModelDriven;

/**
 * Universal CRUD action from book Struts2 in Action
 * 
 * @author oleksii.mogylin
 * 
 * @param <T>
 *            type of model object
 * @param <V>
 *            key type used in model object.
 */
public abstract class BaseCRUDAction<T, V> extends HibernateValidatedAction implements ModelDriven<T> {

    private static final long serialVersionUID = 6158998111468127336L;

    protected CRUDService<T, V> db;
    protected static Logger log = Logger.getLogger(BaseCRUDAction.class);
    private V requestId;
    private boolean readOnly=false;
    private String mappedRequest;

    @SkipValidation
    public String show() {
        setReadOnly(true);
        setMappedRequest(CRUDActionConstants.LIST);
        return SUCCESS;
    }

    @SkipValidation
    public String add() {
        setMappedRequest(CRUDActionConstants.SAVE);
        return SUCCESS;
    }

    public String save() { // insert
        db.add(getModel());
        return redirectTolist();
    }

    @SkipValidation
    public String edit() {
        setMappedRequest(CRUDActionConstants.UPDATE);
        return SUCCESS;
    }

    public String update() {// update
        db.save(getModel());
        return redirectTolist();
    }

    @SkipValidation
    public String destroy() {
        setReadOnly(true);
        setMappedRequest(CRUDActionConstants.REMOVE);
        return CRUDActionConstants.SUCCESS;
    }

    @SkipValidation
    public String remove() {// delete
        db.remove(getModel());
        return redirectTolist();
    }

    @SkipValidation
    public String list() {// code to fetch list objects is in Tiles Controller
        setMappedRequest(CRUDActionConstants.LIST);
        return CRUDActionConstants.LIST;
    }

    @Override
    @SkipValidation
    public String execute() {// default
        return list();
    }

    protected String redirectTolist() {// code to fetch list objects is in Tiles
        // Controller
        setMappedRequest(CRUDActionConstants.LIST);
        return CRUDActionConstants.REDIRECT_LIST;
    }

    public String getActionClass() {
        return getClass().getSimpleName();
    }

    public String getDestination() {
        return getClass().getSimpleName();
    }

    public String getActionMethod() {
        return mappedRequest;
    }

    // when invalid, the request parameter will restore command action
    public void setActionMethod(final String method) {
        this.mappedRequest = method;
    }

    // this prepares command for button on initial screen write
    public void setMappedRequest(final String actionMethod) {
        this.mappedRequest = actionMethod;
        log.debug("setting mappedRequest to " + actionMethod);
    }

    public void setReadOnly(final boolean readOnly) {
        this.readOnly = readOnly;
        log.debug("setting readOnly to " + readOnly);
    }

    public V getRequestId() {
        return requestId;
    }

    public void setRequestId(final V requestId) {
        this.requestId = requestId;
    }

    public void setDb(final CRUDService<T, V> db) {
        this.db = db;
    }

    public CRUDService<T, V> getDb() {
        return this.db;
    }

    public boolean isReadOnly() {
        return readOnly;
    }

    @Override
    public abstract T getModel();

}
