package ua.mogylin.checkpoint.web.action;

/**
 * Constants for results of BaseCRUDAction
 * 
 * @author oleksii.mogylin
 * 
 */
public interface CRUDActionConstants {
    String LIST = "list";
    String REDIRECT_LIST = "redirect_list";
    String SAVE = "save";
    String UPDATE = "update";
    String REMOVE = "remove";
    String SUCCESS = "success";
}
