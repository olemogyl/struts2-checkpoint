package ua.mogylin.checkpoint.web.action;

import java.lang.annotation.ElementType;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.groups.Default;

import org.hibernate.validator.internal.metadata.descriptor.ConstraintDescriptorImpl;
import org.springframework.beans.factory.annotation.Autowired;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

public abstract class HibernateValidatedAction extends ActionSupport {

    private static final long serialVersionUID = 8400626787572529168L;

    @Autowired
    protected Validator hibernateValidator;

    @Override
    public void validate() {

        hibernateValidation(this);

        if (this instanceof ModelDriven) {
            hibernateValidation(((ModelDriven<?>) this).getModel());
        }
    }

    private void hibernateValidation(final Object toValidate) {

        Set<ConstraintViolation<Object>> errors = hibernateValidator.validate(toValidate, new Class[] { Default.class });

        for (ConstraintViolation<Object> violation : errors) {
            ElementType type = ElementType.TYPE;
            if (violation.getConstraintDescriptor() instanceof ConstraintDescriptorImpl) {
                type = ((ConstraintDescriptorImpl<?>) violation.getConstraintDescriptor()).getElementType();
            }
            if (ElementType.FIELD.equals(type) || ElementType.METHOD.equals(type)) {
                this.addFieldError(violation.getPropertyPath().toString(), violation.getMessage());
            } else if (ElementType.TYPE.equals(type)) {
                this.addActionError(violation.getMessage());
            } else {
                throw new UnsupportedOperationException("JSR-303 constraints on " + type
                        + " level are not supported. Only [FIELD, METHOD, TYPE] are supported.");
            }
        }
    }
}
