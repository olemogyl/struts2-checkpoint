package ua.mogylin.checkpoint.web.action;

import static ua.mogylin.checkpoint.web.ApplicationConstants.DEFAULT_PACKAGE;
import static ua.mogylin.checkpoint.web.ApplicationConstants.ROOT_NAMESPACE;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.SessionAware;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.springframework.beans.factory.annotation.Autowired;

import ua.mogylin.checkpoint.model.Login;
import ua.mogylin.checkpoint.service.UserService;
import ua.mogylin.checkpoint.web.util.SessionUtils;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.validator.annotations.VisitorFieldValidator;


@Namespace(ROOT_NAMESPACE)
@ParentPackage(DEFAULT_PACKAGE)
@Results({ @Result(name = "redirect_home", type = "redirectAction", params = { "actionName", "home" }), @Result(name = "home", location = "home"),
    @Result(name = "input", location = "login"), @Result(name = "welcome", location = "welcome"),
    @Result(name = "redirect_welcome", type = "redirectAction", params = { "actionName", "welcome" }),
    @Result(name = "redirect", type = "redirect", location = "${from}") })
public class LoginAction extends ActionSupport implements ModelDriven<Login>, SessionAware {

    private static final long serialVersionUID = 8902841712787942749L;

    @Autowired
    private UserService userService;

    private Map<String, Object> session;

    private Login user = new Login();

    private String from;

    @VisitorFieldValidator(appendPrefix = false)
    public Login getUser() {
        return user;
    }

    @Action("")
    @SkipValidation
    public String showStartPage() {
        if (SessionUtils.getCurrentUser(session) != null) {
            return "home";
        }
        return "welcome";
    }

    @Action("home")
    @SkipValidation
    public String showHomePage() {
        return "home";
    }

    @Action("loginForm")
    @SkipValidation
    public String showLoginForm() {
        return "input";
    }

    @Action("login")
    public String processLoginForm() {
        session.put("user", userService.get(user.getLogin()));
        if (StringUtils.length(from) > 0) {
            return "redirect";
        }
        return "redirect_home";
    }

    @Action("logout")
    @SkipValidation
    public String processLogout() {
        session.remove("user");
        return "redirect_welcome";
    }

    @Override
    public void validate() {
        if (!userService.checkLoginValid(user)) {
            this.addActionError("Sorry, login or password are wrong.");
        }
    }

    @Override
    public Login getModel() {
        return user;
    }

    @Override
    public void setSession(final Map<String, Object> session) {
        this.session = session;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(final String from) {
        this.from = from;
    }

    UserService getUserService() {
        return userService;
    }

    void setUserService(final UserService userService) {
        this.userService = userService;
    }

}
