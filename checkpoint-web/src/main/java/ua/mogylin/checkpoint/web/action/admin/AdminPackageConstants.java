package ua.mogylin.checkpoint.web.action.admin;

/**
 * String constants common for admin package.
 * 
 * @author oleksii.mogylin
 * 
 */
public interface AdminPackageConstants {

    final String ADMIN_ROOT_VIEW_CODE = "admin_home";

    final String APP_DEPARTMENTS_ADMIN_PACKAGE = "webAppDepartmentsAdminPackage";
    final String APP_WORKERS_ADMIN_PACKAGE = "webAppWorkersAdminPackage";
    final String APP_USERS_ADMIN_PACKAGE = "webAppUsersAdminPackage";

}
