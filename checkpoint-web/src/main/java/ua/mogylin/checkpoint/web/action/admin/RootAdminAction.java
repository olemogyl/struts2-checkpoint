package ua.mogylin.checkpoint.web.action.admin;

import static ua.mogylin.checkpoint.web.ApplicationConstants.APP_ADMIN_PACKAGE;
import static ua.mogylin.checkpoint.web.action.admin.AdminPackageConstants.ADMIN_ROOT_VIEW_CODE;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Actions;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionSupport;

@ParentPackage(APP_ADMIN_PACKAGE)
@Results({ @Result(name = ADMIN_ROOT_VIEW_CODE, location = "admin_intro") })
public class RootAdminAction extends ActionSupport {

    private static final long serialVersionUID = -2160595121925084795L;

    /**
     * These produce: empty action, "index", "root-admin"
     * 
     * @return
     */
    @Actions({ @Action(""), @Action("index"), @Action })
    public String getAdminHome() {
        return ADMIN_ROOT_VIEW_CODE;
    }

}
