package ua.mogylin.checkpoint.web.action.admin.departments;

import static com.opensymphony.xwork2.Action.INPUT;
import static com.opensymphony.xwork2.Action.SUCCESS;
import static ua.mogylin.checkpoint.web.action.admin.departments.DepartmentActionUtils.getPossibleParentDepartmentsList;

import java.util.List;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.springframework.beans.factory.annotation.Autowired;

import ua.mogylin.checkpoint.model.Department;
import ua.mogylin.checkpoint.service.DepartmentService;
import ua.mogylin.checkpoint.web.action.HibernateValidatedAction;
import ua.mogylin.checkpoint.web.model.DepartmentWeb;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.interceptor.PreResultListener;
import com.opensymphony.xwork2.validator.annotations.VisitorFieldValidator;

/**
 * Shows the input form for editing department ('view' action) and saves any
 * updates made to the object('save' action)
 * 
 * @author oleksii.mogylin
 * 
 */
@Results({
    @Result(name = INPUT, location = "departmentDetails"),
    @Result(name = SUCCESS, type = "redirectAction", params = { "actionName", "view", "id", "${id}" }) })
public class ChangeDepartmentAction extends HibernateValidatedAction implements
Preparable, ModelDriven<DepartmentWeb>, PreResultListener {

    private static final long serialVersionUID = 3083829310283977999L;
    private static final Logger LOG = Logger.getLogger(ChangeDepartmentAction.class);

    private DepartmentWeb department;

    @Autowired
    protected DepartmentService departmentService;

    @VisitorFieldValidator(appendPrefix = false)
    public DepartmentWeb getDepartment() {
        return department;
    }

    private List<Department> departmentsList;

    public List<Department> getDepartmentsList() {
        return departmentsList;
    }

    @Override
    public DepartmentWeb getModel() {
        if (department == null) {
            department = new DepartmentWeb();
        }
        LOG.info("Model update: " + department);
        return department;
    }

    @Override
    public void prepare() throws Exception {
        departmentsList = getPossibleParentDepartmentsList(departmentService);
    }

    /**
     * view is to show details
     * 
     * @return
     */
    @Action("view")
    @SkipValidation
    public String view() {
        department = DepartmentWeb.toWeb(departmentService.get(department.getId()));
        return INPUT;
    }

    /**
     * save is to save changes
     * 
     * @return
     */
    @Action("save")
    public String save() {
        departmentService.save(department);
        return SUCCESS;
    }

    @Override
    public void beforeResult(final ActionInvocation invocation,
            final String resultCode) {
        excludeCurrentDepartmentFromParents();
    }

    private void excludeCurrentDepartmentFromParents() {
        int toRemove = -1;
        for (int i = 0; i < departmentsList.size(); i++) {
            if (departmentsList.get(i).getId().equals(department.getId())) {
                toRemove = i;
                break;
            }
        }
        departmentsList.remove(toRemove);
    }


}
