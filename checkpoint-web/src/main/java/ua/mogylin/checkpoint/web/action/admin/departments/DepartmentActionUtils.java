package ua.mogylin.checkpoint.web.action.admin.departments;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ua.mogylin.checkpoint.model.Department;
import ua.mogylin.checkpoint.service.DepartmentService;

/**
 * Utility methods to calculate list of possible parent departments.
 * 
 * @author oleksii.mogylin
 * 
 */
public final class DepartmentActionUtils {

    public static Map<Integer, String> getPossibleParentDepartments(final DepartmentService departmentService) {
        return getPossibleParentDepartments(getPossibleParentDepartmentsList(departmentService));
    }

    public static Map<Integer, String> getPossibleParentDepartments(final List<Department> possibleParentDepartmentsList) {
        Map<Integer, String> departmentsMap = new LinkedHashMap<Integer, String>();
        List<Department> departmentsListIntl = new LinkedList<Department>();
        if (!possibleParentDepartmentsList.contains(Department.PSEUDO_ROOT_DEPARTMENT)) {
            departmentsListIntl.add(Department.PSEUDO_ROOT_DEPARTMENT);
        }
        departmentsListIntl.addAll(possibleParentDepartmentsList);
        for (Department dept : departmentsListIntl) {
            departmentsMap.put(dept.getId(), dept.getName());
        }
        return departmentsMap;
    }

    public static List<Department> getPossibleParentDepartmentsList(final DepartmentService departmentService) {
        List<Department> departmentsList = new LinkedList<Department>();
        departmentsList.add(Department.PSEUDO_ROOT_DEPARTMENT);
        departmentsList.addAll(departmentService.getAll());
        return departmentsList;
    }

    private DepartmentActionUtils() {

    }
}
