package ua.mogylin.checkpoint.web.action.admin.departments;

import static ua.mogylin.checkpoint.web.action.admin.departments.DepartmentActionUtils.getPossibleParentDepartments;

import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Actions;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import ua.mogylin.checkpoint.model.Department;
import ua.mogylin.checkpoint.service.DepartmentService;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

/**
 * Displays the list of existing departments ('list' or empty action)
 * 
 * @author oleksii.mogylin
 * 
 */
@Results({ @Result(name = "list", location = "departmentsList") })
public class ListDepartmentsAction extends ActionSupport implements
Preparable {

    private static final long serialVersionUID = -284029968589301115L;

    @Autowired
    protected DepartmentService departmentService;

    private List<Department> departmentsList;
    private Map<Integer, String> departmentsMap;

    public Map<Integer, String> getDepartmentsMap() {
        return departmentsMap;
    }

    public List<Department> getDepartmentsList() {
        return departmentsList;
    }

    @Override
    public void prepare() throws Exception {
        departmentsList = departmentService.getAll();
        departmentsMap = getPossibleParentDepartments(departmentsList);
    }

    @Actions({ @Action("list"), @Action("") })
    public String list() {
        return "list";
    }


}
