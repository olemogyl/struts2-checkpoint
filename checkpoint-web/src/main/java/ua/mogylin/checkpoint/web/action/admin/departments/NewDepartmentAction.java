package ua.mogylin.checkpoint.web.action.admin.departments;

import static com.opensymphony.xwork2.Action.INPUT;
import static com.opensymphony.xwork2.Action.SUCCESS;
import static ua.mogylin.checkpoint.web.action.admin.departments.DepartmentActionUtils.getPossibleParentDepartmentsList;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.springframework.beans.factory.annotation.Autowired;

import ua.mogylin.checkpoint.model.Department;
import ua.mogylin.checkpoint.service.DepartmentService;
import ua.mogylin.checkpoint.web.action.HibernateValidatedAction;
import ua.mogylin.checkpoint.web.model.DepartmentWeb;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;
import com.opensymphony.xwork2.interceptor.ValidationErrorAware;
import com.opensymphony.xwork2.validator.annotations.VisitorFieldValidator;

/**
 * Shows the input form for new department (new action) and saves/adds new
 * department(add action)
 * 
 * @author oleksii.mogylin
 * 
 */
@Results({
    @Result(name = INPUT, location = "newDepartment"),
    @Result(name = SUCCESS, type = "redirectAction", params = { "actionName", "view", "id", "${id}" }),
    @Result(name = "list", type = "redirectAction", params = { "actionName", "list" }) })
public class NewDepartmentAction extends HibernateValidatedAction implements
ModelDriven<DepartmentWeb>, Preparable, ValidationErrorAware {

    private static final long serialVersionUID = 4938680460568618375L;

    @Autowired
    protected DepartmentService departmentService;

    private DepartmentWeb department;

    @VisitorFieldValidator(appendPrefix = false)
    public DepartmentWeb getDepartment() {
        return department;
    }

    private List<Department> departmentsList;

    public List<Department> getDepartmentsList() {
        return departmentsList;
    }

    @Override
    public DepartmentWeb getModel() {
        if (department == null) {
            department = new DepartmentWeb();
        }
        return department;
    }

    @Override
    public void prepare() throws Exception {
        // Not used
    }

    public void prepareShow() {
        departmentsList = getPossibleParentDepartmentsList(departmentService);
    }

    /**
     * new is just to show the form.
     * 
     * @return
     */
    @Action("new")
    @SkipValidation
    public String show() {
        return INPUT;
    }

    /**
     * add is to store object
     * 
     * @return
     */
    @Action("add")
    public String save() {
        department = DepartmentWeb.toWeb(departmentService.add(department));
        return SUCCESS;
    }

    @Override
    public String actionErrorOccurred(final String currentResultName) {
        prepareShow();
        return currentResultName;
    }

}
