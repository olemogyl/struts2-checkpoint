package ua.mogylin.checkpoint.web.action.admin.departments;

import javax.validation.constraints.NotNull;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.beans.factory.annotation.Autowired;

import ua.mogylin.checkpoint.model.Department;
import ua.mogylin.checkpoint.service.DepartmentService;
import ua.mogylin.checkpoint.web.action.HibernateValidatedAction;

import com.opensymphony.xwork2.interceptor.ValidationErrorAware;

/**
 * Removes a department record.
 * 
 * @author oleksii.mogylin
 * 
 */
@Results({ @Result(name = "list", type = "redirectAction", params = { "actionName", "list" }) })
public class RemoveDepartmentAction extends HibernateValidatedAction implements ValidationErrorAware {

    private static final long serialVersionUID = 4793199893980176191L;

    @Autowired
    protected DepartmentService departmentService;

    @NotNull
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    @Action("remove")
    public String remove() {
        departmentService.remove(Department.byId(getId()));
        return "list";
    }

    @Override
    public String actionErrorOccurred(final String currentResultName) {
        return "soft_error";
    }

}
