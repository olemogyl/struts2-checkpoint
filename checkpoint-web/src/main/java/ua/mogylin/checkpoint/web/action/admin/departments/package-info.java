/**
 * 
 */
/**
 * @author oleksii.mogylin
 *
 */
@ParentPackage(APP_DEPARTMENTS_ADMIN_PACKAGE)
package ua.mogylin.checkpoint.web.action.admin.departments;

import static ua.mogylin.checkpoint.web.action.admin.AdminPackageConstants.APP_DEPARTMENTS_ADMIN_PACKAGE;

import org.apache.struts2.convention.annotation.ParentPackage;

