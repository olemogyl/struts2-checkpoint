package ua.mogylin.checkpoint.web.action.admin.users;

import static ua.mogylin.checkpoint.web.action.admin.AdminPackageConstants.APP_USERS_ADMIN_PACKAGE;

import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;

import ua.mogylin.checkpoint.model.User;
import ua.mogylin.checkpoint.service.CRUDService;
import ua.mogylin.checkpoint.service.PersonService;
import ua.mogylin.checkpoint.service.UserService;
import ua.mogylin.checkpoint.web.action.BaseCRUDAction;
import ua.mogylin.checkpoint.web.interceptor.PostParamsPreparable;

import com.opensymphony.xwork2.Preparable;

@ParentPackage(APP_USERS_ADMIN_PACKAGE)
public class UserAction extends BaseCRUDAction<User, String> implements Preparable, PostParamsPreparable {

    private static final long serialVersionUID = 6722333460292854675L;

    private User user;

    @Autowired
    private PersonService personService;

    public PersonService getPersonService() {
        return personService;
    }

    public void setPersonService(final PersonService personService) {
        this.personService = personService;
    }

    @Override
    @Autowired
    public void setDb(final CRUDService<User, String> db) {
        super.setDb(db);
    }

    @Override
    public User getModel() {
        return user;
    }

    @Override
    public void prepare() throws Exception {
        if (user == null) {
            user = new User();
        }
    }

    @Override
    public void preparePostParams() {
        String requestId = this.getRequestId();
        if (requestId != null) {
            LOG.debug("prepare Person from db");
            Integer personId = Integer.valueOf(requestId);
            user = ((UserService) db).getByPersonId(personId);
        }
    }

}
