package ua.mogylin.checkpoint.web.action.admin.workers;

import static ua.mogylin.checkpoint.web.action.admin.AdminPackageConstants.APP_WORKERS_ADMIN_PACKAGE;

import org.apache.log4j.Logger;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.springframework.beans.factory.annotation.Autowired;

import ua.mogylin.checkpoint.model.Person;
import ua.mogylin.checkpoint.service.CRUDService;
import ua.mogylin.checkpoint.service.DepartmentService;
import ua.mogylin.checkpoint.web.action.BaseCRUDAction;
import ua.mogylin.checkpoint.web.interceptor.PostParamsPreparable;

import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

@ParentPackage(APP_WORKERS_ADMIN_PACKAGE)
public class PersonAction extends BaseCRUDAction<Person, Integer> implements Preparable, ModelDriven<Person>, PostParamsPreparable {

    private static final long serialVersionUID = -9103595921576576787L;

    private static final Logger LOG = Logger.getLogger(PersonAction.class);

    private Person person;

    @Autowired
    private DepartmentService departmentService;

    public DepartmentService getDepartmentService() {
        return departmentService;
    }

    public void setDepartmentService(final DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @Override
    @Autowired
    public void setDb(final CRUDService<Person, Integer> db) {
        super.setDb(db);
    }

    @Override
    public void prepare() throws Exception {
        if (person == null) {
            LOG.debug("prepare model Person");
            person = new Person();
        }
    }

    @Override
    public Person getModel() {
        return person;
    }

    @Override
    public void preparePostParams() {
        Integer requestId = this.getRequestId();
        if (requestId != null) {
            LOG.debug("prepare Person from db");
            person = db.get(requestId);
        }
    }

    @Override
    @Action("add")
    public String add() {
        return super.add();
    }

    @Override
    @Action("save")
    public String save() {
        return super.save();
    }

    @Override
    @Action("edit")
    public String edit() {
        return super.edit();
    }

    @Override
    @Action("update")
    public String update() {
        return super.update();
    }

    @Override
    @Action("remove")
    public String remove() {
        return super.remove();
    }

    @Override
    @Action("list")
    public String list() {
        return super.list();
    }

    @Override
    @Action("")
    public String execute() {
        return super.execute();
    }

}
