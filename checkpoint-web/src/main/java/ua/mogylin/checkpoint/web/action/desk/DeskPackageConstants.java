package ua.mogylin.checkpoint.web.action.desk;

/**
 * String constants common for admin package.
 * 
 * @author oleksii.mogylin
 * 
 */
public interface DeskPackageConstants {

    final String DESK_ROOT_VIEW_CODE = "desk_home";

}
