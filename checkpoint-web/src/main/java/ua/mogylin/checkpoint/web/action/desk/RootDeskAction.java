package ua.mogylin.checkpoint.web.action.desk;

import static ua.mogylin.checkpoint.web.ApplicationConstants.DESK_PACKAGE;
import static ua.mogylin.checkpoint.web.action.desk.DeskPackageConstants.DESK_ROOT_VIEW_CODE;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Actions;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionSupport;

@ParentPackage(DESK_PACKAGE)
@Results({ @Result(name = DESK_ROOT_VIEW_CODE, location = "desk_intro") })
public class RootDeskAction extends ActionSupport {

    private static final long serialVersionUID = -2160595121925084795L;

    /**
     * These produce: empty action, "index", "root-admin"
     * 
     * @return
     */
    @Actions({ @Action(""), @Action("index"), @Action })
    public String getAdminHome() {
        return DESK_ROOT_VIEW_CODE;
    }

}
