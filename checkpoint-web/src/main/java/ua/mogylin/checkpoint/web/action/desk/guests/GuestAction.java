package ua.mogylin.checkpoint.web.action.desk.guests;

import static ua.mogylin.checkpoint.web.ApplicationConstants.DESK_PACKAGE;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.interceptor.validation.SkipValidation;
import org.springframework.beans.factory.annotation.Autowired;

import ua.mogylin.checkpoint.model.Guest;
import ua.mogylin.checkpoint.service.CRUDService;
import ua.mogylin.checkpoint.service.PersonService;
import ua.mogylin.checkpoint.web.action.BaseCRUDAction;
import ua.mogylin.checkpoint.web.interceptor.PostParamsPreparable;

import com.opensymphony.xwork2.Preparable;

@ParentPackage(DESK_PACKAGE)
public class GuestAction extends BaseCRUDAction<Guest, Integer> implements Preparable, PostParamsPreparable {

    private static final long serialVersionUID = 6722333460292854675L;

    private Guest guest;

    @Autowired
    private PersonService personService;

    public PersonService getPersonService() {
        return personService;
    }

    @Override
    @Autowired
    public void setDb(final CRUDService<Guest, Integer> db) {
        super.setDb(db);
    }

    @Override
    public Guest getModel() {
        return guest;
    }

    @Override
    public void prepare() throws Exception {
        if (guest == null) {
            guest = new Guest();
        }
    }

    @Override
    public void preparePostParams() {
        Integer requestId = this.getRequestId();
        if (requestId != null) {
            LOG.debug("prepare Person from db");
            guest = db.get(requestId);
        }
    }

    @Override
    @Action("checkin")
    public String add() {
        return super.add();
    }

    @Override
    @Action("save")
    public String save() {
        guest.setCheckinDate(new Date(System.currentTimeMillis()));
        return super.save();
    }

    @Override
    @Action("edit")
    public String edit() {
        setReadOnly(true);
        return super.edit();
    }

    @Override
    @Action("update")
    @SkipValidation
    public String update() {
        return redirectTolist();
    }

    @Override
    @Action("checkout")
    public String remove() {
        guest.setCheckoutDate(new Date(System.currentTimeMillis()));
        return super.save();
    }

    @Override
    @Action("list")
    public String list() {
        return super.list();
    }

    @Override
    @Action("")
    public String execute() {
        return super.execute();
    }

}
