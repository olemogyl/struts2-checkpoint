package ua.mogylin.checkpoint.web.action.ui;

import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.opensymphony.xwork2.ActionSupport;

@Namespace("/ui-common")
@Results({ @Result(location = "common-page-header.jsp") })
public class CommonPageHeaderAction extends ActionSupport {

    /**
     * 
     */
    private static final long serialVersionUID = 4746673163181349650L;

}
