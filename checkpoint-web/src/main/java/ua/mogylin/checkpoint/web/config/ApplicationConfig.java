package ua.mogylin.checkpoint.web.config;

import java.util.Arrays;
import java.util.List;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.apache.log4j.Logger;
import org.hibernate.validator.HibernateValidator;
import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;
import org.hibernate.validator.resourceloading.AggregateResourceBundleLocator;
import org.hibernate.validator.spi.resourceloading.ResourceBundleLocator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;

import ua.mogylin.checkpoint.config.DataServiceConfig;
import ua.mogylin.checkpoint.config.PersistenceConfig;
import ua.mogylin.checkpoint.config.PropertiesConfig;

@Configuration
@EnableAspectJAutoProxy
@Import({ PropertiesConfig.class, PersistenceConfig.class, DataServiceConfig.class })
@ComponentScan(basePackages = { "ua.mogylin.checkpoint.aspect", "ua.mogylin.checkpoint.action", "ua.mogylin.checkpoint.rest.jersey" })
public class ApplicationConfig {

    private static final Logger LOG = Logger.getLogger(ApplicationConfig.class.getName());

    @Bean
    public Validator hibernateValidator() {
        List<String> messageSources = Arrays.asList("global-messages", "ValidationMessages");
        ResourceBundleLocator myCustomResourceBundleLocator = new AggregateResourceBundleLocator(messageSources);
        ValidatorFactory validatorFactory = Validation.byProvider(HibernateValidator.class).configure()
                .messageInterpolator(new ResourceBundleMessageInterpolator(myCustomResourceBundleLocator))
                .buildValidatorFactory();
        Validator validator = validatorFactory.getValidator();

        LOG.info("configured hibernateValidator...");

        return validator;
    }

}
