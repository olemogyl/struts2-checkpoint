package ua.mogylin.checkpoint.web.interceptor;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;

import ua.mogylin.checkpoint.model.User;
import ua.mogylin.checkpoint.web.util.SessionUtils;
import ua.mogylin.struts2.interceptor.AbstractPreActionInterceptor;

import com.opensymphony.xwork2.ActionInvocation;

public class AuthenticationInterceptor extends AbstractPreActionInterceptor {

    public class URIStringWrapper {
        private String lastAcessedURI;

        public URIStringWrapper(final String uri) {
            lastAcessedURI = uri;
        }

        public String getLastAcessedURI() {
            return lastAcessedURI;
        }
    }

    private static final long serialVersionUID = 1852434137899720487L;

    @Override
    protected String interceptBeforeAction(final ActionInvocation invocation)
            throws Exception {
        HttpServletRequest request = ServletActionContext.getRequest();
        User user = SessionUtils.getCurrentUser(invocation.getInvocationContext()
                .getSession());
        if (user == null) {
            String requestURI = request.getRequestURI().replaceFirst(request.getContextPath(), "");
            if (StringUtils.length(requestURI) > 0 && !"/".equals(requestURI)) {
                invocation.getStack().push(new URIStringWrapper(requestURI));
                return "loginAndRedirect";
            }
            return com.opensymphony.xwork2.Action.LOGIN;
        }
        return null;
    }
}
