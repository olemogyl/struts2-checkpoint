package ua.mogylin.checkpoint.web.interceptor;

import org.springframework.beans.factory.annotation.Autowired;

import ua.mogylin.checkpoint.model.LoginType;
import ua.mogylin.checkpoint.model.User;
import ua.mogylin.checkpoint.service.UserService;
import ua.mogylin.checkpoint.web.util.SessionUtils;
import ua.mogylin.struts2.interceptor.AbstractPreActionInterceptor;

import com.opensymphony.xwork2.ActionInvocation;

public class AuthorizationInterceptor extends AbstractPreActionInterceptor {

    private static final long serialVersionUID = -6972750320993685184L;

    @Autowired
    private UserService userService;

    @Override
    protected String interceptBeforeAction(final ActionInvocation invocation) throws Exception {

        User user = SessionUtils.getCurrentUser(invocation.getInvocationContext().getSession());

        if (user == null) {
            return com.opensymphony.xwork2.Action.LOGIN;
        } else if (user.getRole() != LoginType.ADMIN) {
            return "unauthorized_access";
        }
        return null;
    }

}
