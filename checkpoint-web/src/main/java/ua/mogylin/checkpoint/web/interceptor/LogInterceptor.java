package ua.mogylin.checkpoint.web.interceptor;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;

import ua.mogylin.struts2.interceptor.AbstractSeparatedInterceptor;

import com.opensymphony.xwork2.ActionInvocation;

public class LogInterceptor extends AbstractSeparatedInterceptor {

    private static final long serialVersionUID = 5030798304666959498L;

    private static final Logger LOG = Logger.getLogger(LogInterceptor.class
            .getName());

    private Level logLevel;

    @Value("${debug.LogInterceptor.logLevel:DEBUG}")
    public void setLogLevel(final String logLevelStr) {
        this.logLevel = Level.toLevel(logLevelStr);
    }

    @Override
    protected String interceptBeforeAction(final ActionInvocation invocation)
            throws Exception {

        invocation.getInvocationContext().getValueStack();

        String actionHandler = invocation.getAction().getClass().getName() + "."
                + invocation.getProxy().getMethod() + "()";

        LOG.log(logLevel, "Called " + getActionName(invocation) + ", handled by ["
                + actionHandler + "]");

        return null;
    }

    private String getActionName(final ActionInvocation invocation) {
        return invocation.getProxy().getNamespace() + "/"
                + invocation.getProxy().getActionName();
    }

    @Override
    protected String interceptAfterAction(final ActionInvocation invocation)
            throws Exception {

        LOG.log(logLevel, "Action " + getActionName(invocation) + " returned ["
                + invocation.getResultCode() + "] code");
        return null;
    }

}