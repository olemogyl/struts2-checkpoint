package ua.mogylin.checkpoint.web.interceptor;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import ua.mogylin.struts2.interceptor.AbstractPreActionInterceptor;

import com.opensymphony.xwork2.ActionInvocation;

/**
 * The interceptor removes empty values for any child objects (parameter name
 * has dot '.' inside) so params interceptor won't create dummy objects.
 * 
 * @author oleksii.mogylin
 * 
 */
public class NullParamsCleanInterceptor extends AbstractPreActionInterceptor {

    private static final long serialVersionUID = -4037725447039123983L;

    private static final Logger LOG = Logger.getLogger(NullParamsCleanInterceptor.class);

    @Override
    protected String interceptBeforeAction(final ActionInvocation invocation) throws Exception {
        List<String> emptyFields = new LinkedList<String>();
        Map<String, Object> parameters = invocation.getInvocationContext().getParameters();
        for (Entry<String, Object> entity : parameters.entrySet()) {
            String key = entity.getKey();
            Object value = entity.getValue();
            if (key.contains(".")) {
                boolean toRemove = (value == null);
                if (value instanceof String && !toRemove) {
                    toRemove = StringUtils.isBlank((String) value);
                }
                if (value instanceof String[] && !toRemove) {
                    String[] arrayValue = (String[]) value;
                    toRemove = (arrayValue.length == 0 || (arrayValue.length == 1 && StringUtils.isBlank(arrayValue[0])));
                }
                if (toRemove) {
                    emptyFields.add(key);
                }
            }
        }
        for(String fieldName: emptyFields){
            parameters.remove(fieldName);
            LOG.warn("empty request parameter removed from request - " + fieldName);
        }
        return null;
    }

}
