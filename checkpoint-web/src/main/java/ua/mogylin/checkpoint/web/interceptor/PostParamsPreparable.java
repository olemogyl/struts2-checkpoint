package ua.mogylin.checkpoint.web.interceptor;

public interface PostParamsPreparable {

    void preparePostParams();

}
