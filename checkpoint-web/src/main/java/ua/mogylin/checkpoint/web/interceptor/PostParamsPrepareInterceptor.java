package ua.mogylin.checkpoint.web.interceptor;

import ua.mogylin.struts2.interceptor.AbstractPreActionInterceptor;

import com.opensymphony.xwork2.ActionInvocation;

public class PostParamsPrepareInterceptor extends AbstractPreActionInterceptor {

    private static final long serialVersionUID = 8159668267993338886L;

    @Override
    protected String interceptBeforeAction(final ActionInvocation invocation) throws Exception {
        Object action = invocation.getAction();
        if (action instanceof PostParamsPreparable) {
            ((PostParamsPreparable) action).preparePostParams();
        }
        return null;
    }

}
