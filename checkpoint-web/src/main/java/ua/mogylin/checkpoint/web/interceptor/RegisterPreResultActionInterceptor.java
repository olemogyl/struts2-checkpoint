package ua.mogylin.checkpoint.web.interceptor;

import ua.mogylin.struts2.interceptor.AbstractPreActionInterceptor;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.PreResultListener;

public class RegisterPreResultActionInterceptor extends
AbstractPreActionInterceptor {

	private static final long serialVersionUID = -5596711691140089775L;

	@Override
	protected String interceptBeforeAction(final ActionInvocation invocation)
			throws Exception {
		if (invocation.getAction() instanceof PreResultListener) {
			invocation.addPreResultListener((PreResultListener) invocation
					.getAction());
		}
		return null;
	}

}
