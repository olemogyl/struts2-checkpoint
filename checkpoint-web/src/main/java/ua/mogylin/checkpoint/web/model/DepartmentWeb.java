package ua.mogylin.checkpoint.web.model;

import javax.xml.bind.annotation.XmlElement;

import ua.mogylin.checkpoint.model.Department;

import com.opensymphony.xwork2.validator.annotations.CustomValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredFieldValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;

public class DepartmentWeb extends Department {

    public static DepartmentWeb toWeb(final Department object) {
        return new DepartmentWeb(object);
    }

    public DepartmentWeb() {
        super();
    }

    private DepartmentWeb(final Department other) {
        super(other);
    }

    @Override
    @RequiredStringValidator(message = "Fill it!", trim = true, shortCircuit = true)
    @CustomValidator(type = "departmentNameUniqueness", message = "Should be unique!!!")
    public void setName(final String name) {
        super.setName(name);
    }

    @Override
    @XmlElement(name = "parentId")
    public Integer getParentDepartmentId() {
        return super.getParentDepartmentId();
    }

    @Override
    @RequiredFieldValidator(shortCircuit = true)
    @CustomValidator(type = "departmentParentIdValidator", message = "Not valid parent id!")
    public void setParentDepartmentId(final Integer parentDepartmentId) {
        super.setParentDepartmentId(parentDepartmentId);
    }

}
