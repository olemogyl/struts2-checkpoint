package ua.mogylin.checkpoint.web.servlet.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

/**
 * Servlet filter that logs any URL called.
 * 
 * @author oleksii.mogylin
 * 
 */
public class UrlLoggingFilter implements Filter {

    private static final Logger LOG = Logger.getLogger(UrlLoggingFilter.class
            .getName());

    @Override
    public void destroy() {
        // Do nothing
    }

    @Override
    public void doFilter(final ServletRequest request,
            final ServletResponse response,
            final FilterChain chain)
                    throws IOException, ServletException {
        LOG.info("URI called: ["
                + ((HttpServletRequest) request).getRequestURI() + "]");
        chain.doFilter(request, response);

    }

    @Override
    public void init(final FilterConfig config) throws ServletException {
        // Do nothing
    }

}
