package ua.mogylin.checkpoint.web.util;

import java.util.Map;

import ua.mogylin.checkpoint.model.User;

public final class SessionUtils {

    private SessionUtils() {
    }

    public static User getCurrentUser(final Map<String, Object> session) {
        return (User) session.get("user");
    }

}
