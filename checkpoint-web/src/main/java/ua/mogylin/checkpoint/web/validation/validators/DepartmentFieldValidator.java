package ua.mogylin.checkpoint.web.validation.validators;

import org.springframework.beans.factory.annotation.Autowired;

import ua.mogylin.checkpoint.service.DepartmentService;
import ua.mogylin.checkpoint.web.model.DepartmentWeb;

import com.opensymphony.xwork2.validator.ValidationException;
import com.opensymphony.xwork2.validator.validators.FieldValidatorSupport;

/**
 * Base Struts Custom validator to validate a field of Department object.
 * 
 * @author oleksii.mogylin
 * 
 * @param <T>
 *            type of validated field.
 */
public abstract class DepartmentFieldValidator<T> extends FieldValidatorSupport {

    @Autowired
    private DepartmentService departmentService;

    protected DepartmentService getDepartmentService() {
        return departmentService;
    }

    public void setDepartmentService(final DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @Override
    public abstract void validate(final Object object) throws ValidationException;

    protected final DepartmentWeb castValidatedObject(final Object object) {
        if (object instanceof DepartmentWeb) {
            return (DepartmentWeb) object;
        } else {
            throw new IllegalArgumentException("Cannot use " + DepartmentNameUniqueValidator.class.getName() + " with object of type "
                    + object.getClass().getName());
        }
    }

    @SuppressWarnings("unchecked")
    protected T castValidatedValue(final Object object) throws ValidationException {
        Object validatedValue = this.getFieldValue(getFieldName(), object);
        if (checkValueTypeCorrect(validatedValue)) {
            return (T) validatedValue;
        } else {
            throw new IllegalArgumentException("Cannot use " + DepartmentNameUniqueValidator.class.getName() + " to validate value of type "
                    + validatedValue.getClass().getName());
        }
    }

    protected abstract boolean checkValueTypeCorrect(Object validatedValue);
}
