package ua.mogylin.checkpoint.web.validation.validators;

import ua.mogylin.checkpoint.model.Department;

import com.opensymphony.xwork2.validator.ValidationException;

/**
 * Struts custom validator that validates name of department to be unique across
 * the database.
 * 
 * @author oleksii.mogylin
 * 
 */
public class DepartmentNameUniqueValidator extends DepartmentFieldValidator<String> {

    @Override
    public void validate(final Object object) throws ValidationException {
        Department department = castValidatedObject(object);
        String validatedValue = castValidatedValue(object);

        Department deptByName = getDepartmentService()
                .getByName(validatedValue);
        Department deptById = null;
        Integer validatedId = department.getId();
        if(validatedId != null){
            deptById = getDepartmentService().get(validatedId);
        }
        boolean isValid = true;
        if ((deptByName != null && validatedId == null)) {
            isValid = false;
        } else if ((deptByName != null && deptById != null && !deptById.getId()
                .equals(deptByName.getId()))) {
            isValid = false;
        }
        if (!isValid) {
            addFieldError(getFieldName(), object);
        }
    }

    @Override
    protected boolean checkValueTypeCorrect(final Object validatedValue) {
        return validatedValue instanceof String;
    }

}
