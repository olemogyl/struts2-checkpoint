package ua.mogylin.checkpoint.web.validation.validators;

import ua.mogylin.checkpoint.model.Department;

import com.opensymphony.xwork2.validator.ValidationException;

/**
 * Struts custom validator that validates parent department id to be valid id of
 * existing department.
 * 
 * @author oleksii.mogylin
 * 
 */
public class DepartmentParentIdValidator extends DepartmentFieldValidator<Integer> {

    @Override
    public void validate(final Object object) throws ValidationException {
        Department department = castValidatedObject(object);
        Integer validatedValue = castValidatedValue(object);

        if (Department.PSEUDO_ROOT_DEPARTMENT.getId().equals(validatedValue)) {
            return;
        }

        Department deptById = getDepartmentService().get(validatedValue);
        if (deptById == null || deptById.getId().equals(department.getId())) {
            addFieldError(getFieldName(), object);
        }
    }

    @Override
    protected boolean checkValueTypeCorrect(final Object validatedValue) {
        return validatedValue instanceof Integer;
    }

}
