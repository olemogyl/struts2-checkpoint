<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<html>
<head>
<!------------------------ title zone ---------------->
<title><tiles:insertAttribute name="title" /></title>
<!---------------------------------------------------->
<link rel=stylesheet type="text/css" href="style/style.css">
</head>
<!------------------------ bodyBackground zone ----------------------->
<body bottommargin="0" leftmargin="0" marginheight="0" marginwidth="0"
	rightmargin="0" topmargin="0"
	background=<tiles:insertAttribute name="bodyBackground"/>>
	<table style='width:780;border-spacing:0;border-collapse:collapse' cellpadding="0" border="0" >
	<tr>
		<td>
			<!-------------------------------------------------------------------->
			<table style='width:100%;height:143;' cellpadding="0" cellspacing="0" border="0">
				<tr valign="top">
					<td width="780">
						<!------------------------ Logo zone ----------------------> 
						<img src="<tiles:insertAttribute name="logo"/>" width="780" height="143" border="0" alt="">
						<!--------------------------------------------------------->
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table style='width:100%;min-height:300;' cellpadding="0" cellspacing="0" border="0">
				<tr valign="top">
					<td>
						<!---------- Body zone --------------->
						<tiles:insertAttribute name="body" />
						<!------------------------------------>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table style='width:100%' cellpadding="0" cellspacing="0" border="0">
				<tr valign="middle" align="center">
					<td>
						<!--------- Footer zone ---------------->
						<tiles:insertAttribute name="footer" />
						<!-------------------------------------->
					</td>
				</tr>
			</table>
		</td>
	</tr>
	</table>
</body>
</html>