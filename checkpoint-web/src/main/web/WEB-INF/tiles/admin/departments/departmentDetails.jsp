<%@ taglib prefix="s" uri="/struts-tags" %>
	<s:set name="lastdeptname" scope="session" value="name"/>
	<s:form method="POST" action="save.do">
		<s:hidden name="id" />
		<s:textfield name="name" label="Department's title" />
		<s:select name="parentDepartmentId" list="departmentsList" listKey="id" listValue="name" label="Parent department"></s:select>
		<s:submit value="Save"></s:submit>
	</s:form>