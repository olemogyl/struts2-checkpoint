<%@ taglib prefix="s" uri="/struts-tags" %>
	<a href="new.do">+ department</a>
	<table style="width:100%">
	<thead>
		<tr>
			<td>Id</td>
			<td>Title</td>
			<td><s:text name="admin.departments.parentlabel"></s:text></td>
			<td>&nbsp;</td>
		</tr>
	</thead>
	<tbody>
	<s:iterator value="departmentsList">
		<s:url action="view" var="viewDeptUrl"><s:param name="id" value="id"/></s:url>
		<s:url action="remove" var="removeDeptUrl"><s:param name="id" value="id"/></s:url>
		<s:url action="view" var="viewDeptUrl2"><s:param name="id" value="parentDepartmentId"/></s:url>
		<tr>
			<td><s:property value="id"/></td>
			<td><a href='<s:property value="#viewDeptUrl" />'><s:property value="name"/></a></td>
			<td><a href='<s:property value="#viewDeptUrl2" />'> ${departmentsMap[parentDepartmentId]} </a></td>
			<td><a href='<s:property value="#removeDeptUrl" />'>Remove</a></td>
		</tr>
	</s:iterator>
	</tbody>
	</table>