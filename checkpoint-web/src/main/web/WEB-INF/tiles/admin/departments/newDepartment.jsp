<%@ taglib prefix="s" uri="/struts-tags" %>
	<s:form method="POST" action="add.do">
		<s:textfield name="name" label="Department's title"></s:textfield>
		<s:select name="parentDepartmentId" list="departmentsList" listKey="id" listValue="name" label="Parent department"></s:select>
		<s:submit value="Save" />
	</s:form>
