<%@ taglib prefix="s" uri="/struts-tags"%>
	<s:url action="%{actionMethod}" var="submitTo"></s:url>
	<s:form action="%{actionMethod}">
		<s:hidden name="id" />
		<s:hidden name="actionMethod" value="%{actionMethod}" />
		<s:textfield name="login" label="Login" readonly="%{readOnly}" />
		<s:textfield name="password" label="Password" readonly="%{readOnly}" />
		<s:select emptyOption="false" name="role" list="%{@ua.mogylin.checkpoint.model.LoginType@values()}" label="Role" readonly="%{readOnly}"></s:select>
		<s:fielderror template="plainFieldError" fieldName="department"></s:fielderror>		
		<s:select emptyOption="false" name="person.id" list="%{personService.getAll()}" listKey="id" listValue="name" label="Worker" readonly="%{readOnly}"></s:select>
		<s:submit action="%{actionMethod}" label="label.ok" />
	</s:form>
