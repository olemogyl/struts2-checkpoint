<%@ taglib prefix="s" uri="/struts-tags"%>
	<a href='<s:url action="add" />'>+ user</a>
	<table style="width:100%"><s:set value="%{departmentService.getAllToMap()}" var="departmentsMap"></s:set>
		<thead>
			<tr>
				<td>Name</td>
				<td>Department</td>
				<td>Login</td>
				<td>Role</td>
				<td>&nbsp;</td>
			</tr>
		</thead>
		<tbody>
			<s:iterator value="%{db.getAll()}">
			<tr><s:url action="remove" var="removeUrl"><s:param name="requestId" value="person.id"/></s:url>
				<td><a href='<s:url action="edit"><s:param name="requestId" value="person.id"/></s:url>'><s:text name="person.lastName"></s:text>, <s:text name="person.firstName"></s:text></a></td>
				<td><s:property value='#departmentsMap[person.department.id]'/></td>
				<td><s:text name="login"></s:text></td>
				<td><s:text name="role"></s:text></td>
				<td><a href='<s:property value="#removeUrl" />'>Remove</a></td>
			</tr></s:iterator>
		</tbody>
	</table>
