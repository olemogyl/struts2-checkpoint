<%@ taglib prefix="s" uri="/struts-tags"%>
	<s:url action="%{actionMethod}" var="submitTo"></s:url>
	<s:form action="%{actionMethod}">
		<s:hidden name="id" />
		<s:hidden name="actionMethod" value="%{actionMethod}" />
		<s:textfield name="firstName" label="First Name" readonly="%{readOnly}" />
		<s:textfield name="lastName" label="Last Name" readonly="%{readOnly}" />
		<s:fielderror template="plainFieldError" fieldName="department"></s:fielderror>		
		<s:select emptyOption="true" name="department.id" list="%{departmentService.getAll()}" listKey="id" listValue="name" label="Department" readonly="%{readOnly}"></s:select>
		<s:textfield name="yearBorn" label="Year" readonly="%{readOnly}" />
		<s:submit action="%{actionMethod}" label="label.ok" />
	</s:form>