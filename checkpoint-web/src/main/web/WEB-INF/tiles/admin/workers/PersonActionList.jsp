<%@ taglib prefix="s" uri="/struts-tags"%>
	<a href='<s:url action="add" />'>+ worker</a>
	<table style="width:100%"><s:set value="%{departmentService.getAllToMap()}" var="departmentsMap"></s:set>
		<thead>
			<tr>
				<td>Name</td>
				<td>Year born</td>
				<td>Department</td>
				<td>&nbsp;</td>
			</tr>
		</thead>
		<tbody>
			<s:iterator value="%{db.getAll()}">
			<tr><s:url action="remove" var="removeUrl"><s:param name="requestId" value="id"/></s:url>
				<td><a href='<s:url action="edit"><s:param name="requestId" value="id"/></s:url>'><s:text name="lastName"></s:text>, <s:text name="firstName"></s:text></a></td>
				<td><s:text name="yearBorn"></s:text></td>
				<td><s:property value='department.name'/></td>
				<td><a href='<s:property value="#removeUrl" />'>Remove</a></td>
			</tr></s:iterator>
		</tbody>
	</table>