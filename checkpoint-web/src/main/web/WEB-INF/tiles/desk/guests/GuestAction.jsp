<%@ taglib prefix="s" uri="/struts-tags"%>
	<s:url action="%{actionMethod}" var="submitTo"></s:url>
	<s:form action="%{actionMethod}">
		<s:hidden name="id" />
		<s:hidden name="actionMethod" value="%{actionMethod}" />
		<s:textfield name="firstName" label="First Name" readonly="%{readOnly}" />
		<s:textfield name="lastName" label="Last Name" readonly="%{readOnly}" />
		
		<s:if test="%{actionMethod == 'update'}">
			<s:textfield name="idType" label="ID Type" readonly="%{readOnly}" />
			<s:textfield name="idNumber" label="ID Number" readonly="%{readOnly}" />
			<s:hidden name="host.id" />
			<s:textfield name="host.name" label="Host" readonly="%{readOnly}" />
		</s:if>
		<s:else>
			<s:select emptyOption="false" name="idType" list="%{@ua.mogylin.checkpoint.model.IdType@values()}" label="ID Type" disabled="%{readOnly}"></s:select>
			<s:textfield name="idNumber" label="ID Number" readonly="%{readOnly}" />
			<s:fielderror template="plainFieldError" fieldName="host"></s:fielderror>		
			<s:select emptyOption="false" name="host.id" list="%{personService.getAll()}" listKey="id" listValue="name" label="Worker" disabled="%{readOnly}"></s:select>
		</s:else>
		<s:if test="%{actionMethod == 'update'}">
			<s:date name="checkoutDate" format="MM/dd/yyyy 'at' hh:mm a" var="checkoutDateFormatted" />
			<s:if test="%{checkoutDate == null}">
				<s:date name="checkinDate" format="MM/dd/yyyy 'at' hh:mm a" var="checkinDateFormatted" nice="true"/>
			</s:if>	
			<s:else>
				<s:date name="checkinDate" format="MM/dd/yyyy 'at' hh:mm a" var="checkinDateFormatted"/>	
			</s:else>
			<s:label label="Checked in" value="%{#checkinDateFormatted}"></s:label>
			<s:label label="Checked in" value="%{#checkoutDateFormatted}"></s:label>
		</s:if>
		<s:submit action="%{actionMethod}" label="label.ok" />
	</s:form>
