<%@ taglib prefix="s" uri="/struts-tags"%>
	<a href='<s:url action="checkin" />'>+ guest</a>
	<table style="width:100%"><s:set value="%{personService.getAllToMap()}" var="workersMap"></s:set>
		<thead>
			<tr>
				<td>Name</td>
				<td>Host</td>
				<td>Check-in</td>
				<td>Check-out</td>
				<td>&nbsp;</td>
			</tr>
		</thead>
		<tbody>
			<s:iterator value="%{db.getAll()}">
			<s:set value="" var="checkinDateFormatted"></s:set>
			<s:set value="" var="checkoutDateFormatted"></s:set>
			<s:date name="checkoutDate" format="MM/dd/yyyy HH:mm" var="checkoutDateFormatted" />
			<s:if test="%{checkoutDate == null}">
				<s:date name="checkinDate" format="MM/dd/yyyy HH:mm" var="checkinDateFormatted" nice="true"/>
			</s:if>	
			<s:else>
				<s:date name="checkinDate" format="MM/dd/yyyy HH:mm" var="checkinDateFormatted"/>	
			</s:else>
			<tr><s:url action="checkout" var="checkoutUrl"><s:param name="requestId" value="id"/></s:url>
				<td><a href='<s:url action="edit"><s:param name="requestId" value="id"/></s:url>'><s:text name="lastName"></s:text>, <s:text name="firstName"></s:text></a></td>
				<td><s:property value='#workersMap[host.id].name'/></td>
				<td><s:property value='%{#checkinDateFormatted}'/></td>
				<td><s:property value='%{#checkoutDateFormatted}'/></td>
				<td>
					<s:if test="%{checkoutDate == null}">
					<a href='<s:property value="#checkoutUrl" />'>Check-out</a>
					</s:if>	
				</td>
			</tr></s:iterator>
		</tbody>
	</table>
