<%@ taglib prefix="s" uri="/struts-tags"%>
<table style="width:100%">
  <tr>
    <td align="center">
		<div style='display:block;'>
			Please enter your credentials.<br/>
			<s:form action="login">
				<s:actionerror/>
				<s:hidden name="from" />
				<s:textfield id="login" name="login" label="Login"></s:textfield><br/>
				<s:password id="password" name="password" label="Password"></s:password>
				<s:submit/>
			</s:form>
		</div>    
    </td>
  </tr>
</table>
