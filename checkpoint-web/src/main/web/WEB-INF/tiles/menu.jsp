<%@ taglib prefix="s" uri="/struts-tags"%>
<s:if test="%{#session['user'].role == @ua.mogylin.checkpoint.model.LoginType@ADMIN}">
	<s:url action="list" namespace="/admin/departments" var="DeptUrl" />
	<p><a href='<s:property value="#DeptUrl" />'>Departments</a></p>
	<s:url action="list" namespace="/admin/workers" var="WorkerUrl" />
	<p><a href='<s:property value="#WorkerUrl" />'>Workers</a></p>
	<s:url action="list" namespace="/admin/users" var="UserUrl" />
	<p><a href='<s:property value="#UserUrl" />'>Users</a></p>
</s:if>
	<s:url action="list" namespace="/desk/guests" var="GuestsUrl" />
	<p><a href='<s:property value="#GuestsUrl" />'>Guests</a></p>
	<s:url action="checkin" namespace="/desk/guests" var="AddGuestUrl" />
	<p><a href='<s:property value="#AddGuestUrl" />'>+ guest</a></p>
	<s:url action="groupCheckin" namespace="/desk/guests" var="AddGroupUrl" />
	<p><a href='<s:property value="#AddGroupUrl" />'>+ group</a></p>