package ua.mogylin.checkpoint.web.action;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Map;

import org.apache.struts2.StrutsSpringJUnit4TestCase;
import org.apache.struts2.dispatcher.mapper.ActionMapping;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;

import ua.mogylin.checkpoint.model.Login;
import ua.mogylin.checkpoint.service.UserService;

import com.opensymphony.xwork2.ActionProxy;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContextBootstrap.xml" })
@TransactionConfiguration(transactionManager = "transactionManager")
@Ignore
public class LoginAction2Test extends StrutsSpringJUnit4TestCase<LoginAction> {

    private UserService userService = mock(UserService.class);

    @Test
    public void testGetActionMapping() {
        ActionMapping mapping = getActionMapping("/login.do");
        assertNotNull(mapping);
        assertEquals("/", mapping.getNamespace());
        assertEquals("login", mapping.getName());
    }

    @Test
    public void testLoginForm() throws Exception {
        final ActionProxy proxy = this.getActionProxy("/login");
        assertNotNull(proxy);

        Map<String, Object> params = proxy.getInvocation().getInvocationContext().getParameters();
        params.put("login", "login");
        params.put("password", "password");
        params.put("from", "test");

        // proxy.getInvocation().getInvocationContext().setSession(new
        // HashMap<String, Object>());

        final LoginAction loginAction = LoginAction.class.cast(proxy.getAction());

        assertNotNull(loginAction.getModel());

        loginAction.setUserService(userService);

        when(userService.checkLoginValid((Login) org.mockito.Matchers.anyObject())).thenReturn(true);

        proxy.setExecuteResult(false);
        assertEquals("actionName result", "redirect", proxy.execute());


    }
}
