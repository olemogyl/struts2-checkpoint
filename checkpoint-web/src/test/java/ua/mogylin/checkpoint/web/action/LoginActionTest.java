package ua.mogylin.checkpoint.web.action;

import org.apache.struts2.StrutsSpringTestCase;
import org.apache.struts2.dispatcher.mapper.ActionMapping;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class LoginActionTest extends StrutsSpringTestCase {

    @Override
    public String[] getContextLocations() {
        return new String[] { "classpath:applicationContextBootstrap.xml" };
    }

    @Test
    public void testGetActionMapping() {
        ActionMapping mapping = getActionMapping("/login.do");
        assertNotNull(mapping);
        assertEquals("/", mapping.getNamespace());
        assertEquals("login", mapping.getName());

    }


}
