package ua.mogylin.checkpoint.ws.config;

import java.util.Arrays;
import java.util.LinkedList;

import javax.ws.rs.Path;

import org.apache.cxf.Bus;
import org.apache.cxf.endpoint.Server;
import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.apache.cxf.jaxrs.lifecycle.ResourceProvider;
import org.apache.cxf.jaxrs.spring.SpringResourceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

import ua.mogylin.checkpoint.config.DataServiceConfig;
import ua.mogylin.checkpoint.config.PersistenceConfig;
import ua.mogylin.checkpoint.config.PropertiesConfig;
import ua.mogylin.checkpoint.ws.endpoint.DepartmentResource;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

@Configuration
@ImportResource("classpath:META-INF/cxf/cxf.xml")
@Import({ PropertiesConfig.class, PersistenceConfig.class, DataServiceConfig.class })
@ComponentScan("ua.mogylin.checkpoint.ws.endpoint")
public class ApplicationConfig {

    @Autowired
    private ApplicationContext ctx;

    @Autowired
    private Bus cxfBus;

    @Autowired
    private Server jaxRsServer;
    // More code

    // @Bean
    // @Autowired
    // public Endpoint departmentResource(final DepartmentResource
    // departmentResourceBean) {
    // EndpointImpl endpoint = new EndpointImpl(cxfBus, departmentResourceBean);
    // endpoint.setAddress("/Departments");
    // endpoint.publish();
    // return endpoint;
    // }

    @Autowired
    private DepartmentResource departmentResource;

    @Bean
    public Server jaxRsServer() {
        LinkedList<ResourceProvider> resourceProviders = new LinkedList<>();
        for (String beanName : ctx.getBeanDefinitionNames()) {
            if (ctx.findAnnotationOnBean(beanName, Path.class) != null) {
                SpringResourceFactory factory = new SpringResourceFactory(beanName);
                factory.setApplicationContext(ctx);
                resourceProviders.add(factory);
            }
        }

        JAXRSServerFactoryBean factory = new JAXRSServerFactoryBean();
        factory.setBus(cxfBus);
        factory.setProviders(Arrays.asList(new JacksonJsonProvider()));
        factory.setResourceProviders(resourceProviders);
        return factory.create();
    }

}