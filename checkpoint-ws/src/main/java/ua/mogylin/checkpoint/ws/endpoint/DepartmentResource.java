package ua.mogylin.checkpoint.ws.endpoint;

import java.util.List;

import javax.jws.WebService;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import ua.mogylin.checkpoint.model.Department;

@WebService
@Consumes("application/json")
@Produces("application/json")
@Path("/departments/")
public interface DepartmentResource {

    @GET
    List<Department> getAll();

    Department get(final Integer id);

    Department addDepartment(final Department department);
}
