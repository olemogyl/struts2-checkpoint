package ua.mogylin.checkpoint.ws.endpoint;

import java.util.List;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ua.mogylin.checkpoint.model.Department;
import ua.mogylin.checkpoint.service.DepartmentService;

@Component
@WebService(endpointInterface = "ua.mogylin.checkpoint.ws.endpoint.DepartmentResource")
public class DepartmentResourceImpl implements DepartmentResource {

    @Autowired
    private DepartmentService departmentService;


    @Override
    public List<Department> getAll() {
        return departmentService.getAll();
    }

    @Override
    public Department get(final Integer id) {
        return departmentService.get(id);
    }

    @Override
    public Department addDepartment(final Department department) {
        return departmentService.add(department);
    }
}
